import React, { Component } from 'react';

import './App.css';
import TaskForm from './components/TaskForm';
import SearchSort from './components/SearchSort';
import TaskList from './components/TaskList';
import _ from 'lodash';
import state from './trainning/states'

class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      tasks:[], // id , name // status ,
      isDisplayForm : false,
      taskEditting : null,
      filter: {
        name:'',
        status:-1
      },
      keyword:'',
      sortBy: 'name',
      sortValue:1
    }
  }

  componentWillMount(){
    // console.log("hi")
    if(localStorage && localStorage.getItem("tasks")){
      var tasks = JSON.parse(localStorage.getItem("tasks"))
      this.setState({tasks:tasks})
    }

  }
  // onGenerateData = () => {
  //   var tasks = [
  //     {
  //       id: this.genID(),
  //       name : "hoc bai",
  //       status : true
  //     },
  //     {
  //       id: this.genID(),
  //       name : "lam bai",
  //       status : false
  //     },
  //     {
  //       id: this.genID(),
  //       name : "giang bai",
  //       status : true
  //     }
  //   ]
  //   localStorage.setItem("tasks",JSON.stringify(tasks))
  //   console.log(tasks)
  // }
  onToggleForm = () => {
    if(this.state.isDisplayForm && this.state.taskEditting !== null){
      this.setState({
        isDisplayForm:true,
        taskEditting : null
      })
    }else{
      this.setState({
        isDisplayForm:!this.state.isDisplayForm,
        taskEditting : null
      })
    }
  }
  onCloseForm = () => {
    this.setState({isDisplayForm:false})
  }

  onOpenForm = () => {
    this.setState({isDisplayForm:true})
  }

  s4(){
    return Math.floor((1+Math.random())* 0x100000).toString(16).substring(1);
  }
  genID(){
    return this.s4() + this.s4() + "-" +this.s4() +this.s4() + "-" +this.s4()+this.s4() + "-" +this.s4()+this.s4() + "-" +this.s4()+this.s4()
  }
  onSubmit = (data) => {
    console.log(data);


    if(data.id===""){
      // add to tasks
      var task = {
        id : this.genID(),
        name : data.name,
        status: data.status
      }
      var {tasks} = this.state;
      tasks.push(task)

    }else{
      var {tasks} = this.state;
      // edit 
      var index = tasks.findIndex(task=> task.id===data.id )
      tasks[index] = data;
    }

    
    this.setState({tasks:tasks,taskEditting:null})
    localStorage.setItem('tasks',JSON.stringify(tasks))
    this.onCloseForm()
    
  }

  // ========== CRUD ====================

  onUpdateStatus = (id) => {
    // console.log(id)
    var {tasks} = this.state;
    // console.log(tasks)
    // var index = tasks.findIndex(task=> task.id===id )
    var index = _.findIndex(tasks,(task)=>{
      return task.id===id 
    })
    tasks[index].status = !tasks[index].status;
    // console.log( tasks[index].status)

    this.setState({tasks})

    localStorage.setItem('tasks',JSON.stringify(tasks))    
  }
  onDelete = (id) => {

    var {tasks} = this.state;
    var index = tasks.findIndex(task=> task.id===id )

    console.log( tasks)
    tasks.splice(index,1)
    console.log( tasks)

    this.setState({tasks})

    localStorage.setItem('tasks',JSON.stringify(tasks))    
  }
  onUpdate = (id) =>{
    console.log(id)
    var {tasks} = this.state;
    var index = tasks.findIndex(task=> task.id===id )
    var taskEditting = tasks[index]
    console.log(taskEditting)
    // this.
    this.setState({taskEditting:taskEditting})
    this.onOpenForm() 
  }
  onFilter= (filterName,filterStatus)=>{
    // console.log(filterName,typeof filterStatus);
    filterStatus = parseInt(filterStatus)
    this.setState({
      filter:{
        name: filterName.toLowerCase(),
        status : filterStatus
            }
    })
    
  }
  onSearch = (keyword)=>{
    // console.log(data)
    this.setState({keyword:keyword})
  }

  onSort=(sortBy,sortValue)=>{
    this.setState({
      sortBy:sortBy,
      sortValue: sortValue
    })
    // console.log(this.state.sortBy,this.state.sortValue)
  }

  render(){
    var { sortBy,
          sortValue,
          tasks, 
          isDisplayForm,
          taskEditting, 
          filter, 
          keyword
        } = this.state // var tasks = this.state.tasks
    // if(filter){
    //   // console.log(filter);
    //   if(filter.name){
    //     tasks = tasks.filter(task=>{
    //       return task.name.toLowerCase().indexOf(filter.name) !== -1
    //     })
    //   }
    // }
    tasks = _.filter(tasks,(task)=>{
      return task.name.toLowerCase().indexOf(filter.name) !== -1
    })


    tasks = tasks.filter(task=>{
      if(filter.status === -1){
        return task
      }else{

        return task.status === (filter.status == 1 ? true:false)
      }
    })



    // if(keyword){
    //   tasks = tasks.filter(task=>{
    //     return task.name.toLowerCase().indexOf(keyword) !== -1
    //   })
    // }
    tasks = _.filter(tasks,(task)=>{
      return task.name.toLowerCase().indexOf(keyword) !== -1
    })


    // console.log( sortBy,sortValue)

    if(sortBy==="name"){
      tasks.sort((a,b)=>{
        if(a.name > b.name) return sortValue
        else if(a.name < b.name) return -sortValue
        else return 0
      })
    }else{
      tasks.sort((a,b)=>{
        if(a.status > b.status) return -sortValue
        else if(a.status < b.status) return sortValue
        else return 0
      })
    }
    
    
    var elmTaskForm = isDisplayForm ? 
                  <TaskForm 
                  task={taskEditting}
                  onSubmit={this.onSubmit}
                  onCloseForm={this.onCloseForm} /> : '';
    return(
      <div className="container">
          <div className="text-center">
              <h1>Quản Lý Công Việc</h1>
              <hr/>
          </div>
          <div className="row">
              <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
              {elmTaskForm}
              </div>
              <div className={isDisplayForm ? "col-xs-8 col-sm-8 col-md-8 col-lg-8" : "col-xs-12 col-sm-12 col-md-12 col-lg-12"}>
                  <button 
                   type="button" 
                   onClick={this.onToggleForm}
                   className="btn btn-primary">
                      <span className="fa fa-plus mr-5"></span>Thêm Công Việc
                  </button>
                  
                  <SearchSort 
                    onSearch={this.onSearch}  
                    onSort={this.onSort}
                    sortBy={sortBy}
                    sortValue={sortValue}
                    />
                  <div className="row mt-15">
                      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <TaskList 
                          onUpdate={this.onUpdate} 
                          onDelete={this.onDelete} 
                          onUpdateStatus={this.onUpdateStatus} 
                          tasks={tasks}
                          onFilter = {this.onFilter}
                        />
                      </div>
                  </div>
              </div>
          </div>
      </div>
    );
  }
}

export default App;
