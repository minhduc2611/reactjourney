import React, { Component } from 'react';



class TaskForm extends Component {
    constructor(props){
        super(props)
        this.state= {
            id:"",
            name:"",
            status: false
        }
            
    }

    UNSAFE_componentWillMount(){
        if(this.props.task){
            this.setState({
                id:this.props.task.id,
                name:this.props.task.name,
                status: this.props.task.status
            })
        }
    }

    // active every props passed in 
    componentWillReceiveProps(nextProps){
        console.log(nextProps)
        if( nextProps && nextProps.task){
            this.setState({
                id: nextProps.task.id,
                name: nextProps.task.name,
                status:  nextProps.task.status
            })
        }else if(!nextProps.task){
            this.setState({
                id: "",
                name: "",
                status:  ""
            })
        }
    }

    onChange = (event) => {
        var target = event.target;
        var name = target.name;
        var value = target.value;
        this.setState({
            [name] : value
        })
    }
    onCloseForm  = () => {
        this.props.onCloseForm()
    }
    onSubmit = (event)=>{
        event.preventDefault()
        this.props.onSubmit(this.state)
        //
        this.onClear()
        this.onCloseForm()
        
    }  
    onClear = ()=>{
        this.setState({
            name:'',
            status:false
          })
        
    }   
render(){
    var {id} = (this.props.task) ? this.props.task : ''
    var onCloseForm = ()=>{this.props.onCloseForm()}
    console.log(id)
    return(
        
<div className="panel panel-warning">
    <div className="panel-heading">
        <h3 className="panel-title">{(id ==='' || id == undefined) ? "Thêm Công Việc ": "Cập nhật công việc"}
            <button  className="fa fa-times-circle text-right" onClick={onCloseForm}></button>
        </h3> 
    </div>
    <div className="panel-body">
        <form onSubmit={this.onSubmit}>
            <div className="form-group">
                <label>Tên :</label>
                <input 
                    type="text" 
                    className="form-control"
                    name="name"
                    // input thif phai luoon cos value và onchange 
                    value={this.state.name}
                    onChange={this.onChange}
                />
            </div>
            <label>Trạng Thái :</label>
            <select 
                value={this.state.status}
                onChange={this.onChange}
                name="status"
                className="form-control" required="required">
                <option value={true}>Kích Hoạt</option>
                <option value={false}>Ẩn</option>
            </select>
            <br/>
            <div className="text-center">
                <button type="submit" className="btn btn-warning">{(id ==='' || id == undefined)? "Thêm ": "Cập nhật"}</button>&nbsp;
                <button onClick={this.onClear} type="button" className="btn btn-danger">Hủy Bỏ</button>
            </div>
        </form>
    </div>
</div>
                
      );
}
  
}

export default TaskForm;
