
import status from './status';
import sort from './sort';
import {combineReducers} from 'redux';


const myReducer = combineReducers({
    status ,
    sort
})

// //b3: tao reducer and states
// var intitialState = {
//     tasks:[], // id , name // status ,
//     isDisplayForm : false,
//     taskEditting : null,
//     filter: {
//       name:'',
//       status:-1
//     },
//     keyword:'',
//     sortBy: 'name',
//     sortValue:1
//   }
// var myReducer = (state = intitialState, action)=>{ // tra ra 1 state moi

//     // b6: reducer remake the states
//     if(action.type === "TOGGLE_FORM"){
//         state.isDisplayForm = !state.isDisplayForm 
//         return state
//     }
//     if(action.type === "SORT"){
//         // console.log(action.sort);
        
//         var {sortBy,sortValue} = action.sort;
//         var {isDisplayForm} = state;
//         // var {status}
        
//         // console.log(state);

//         return {
//             isDisplayForm,
//             sortBy : sortBy ,
//             sortValue : sortValue
//             }
//     }
//     return state
// }

export default myReducer