// console.log("helo redux")

import {createStore} from 'redux';
import {status, sortAction} from './actions/index'
import myReducer from './reducers/index'


// b1: tao ra store

const store = createStore(myReducer); //b4 cho vafo store reducer

console.log(store.getState());

// b5 thuc hien thay doi state: store goi dispatcher, send actions to reducer

// var action = {type: "TOGGLE_FORM"};
// store.dispatch(action);
store.dispatch(status());




// /b2 sd = casch log store

console.log('toggle:');
console.log(store.getState());


// THUC HEN CONG VIEC SAP XEP Z-> A
// var sortAction = {
//     type:"SORT",
//     sortBy: 'status',
//     sortValue: -1
// }
store.dispatch(sortAction({
    sortBy:'status',
    sortValue: -1
}))
console.log('sortAction:');
console.log(store.getState());
