import * as types from './../constance/index';

export const status = ()=>{
    return {
        type: types.TOGGLE_FORM
    }
}

export const sortAction = (sort)=>{
    return {
        type: types.SORT,
        sort
    }
}