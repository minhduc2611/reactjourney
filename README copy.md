
#The difference between imperative and declarative programming
One of the reasons why React is so powerful is because it enforces a declarative programming paradigm. 
Imperative programming as a way of describing how things work, and declarative programming as a way of describing what you want to achieve.

A React component to show a map on a page would look like this instead:
    <Gmaps zoom={4} center={myLatLng}>
        <Marker position={myLatLng} title="Hello world!" />
    </Gmaps>
In declarative programming, developers only describe what they want to achieve, and there's no need to list all the steps to make it work.
and there's no need to tell it how to interact with the DOM; you declare what you want to see on the screen and React does the job for you.

#React components and their instances, and how React uses elements to control the UI flow

#How React changes the way we build web applications, enforcing a different new concept of separation of concerns, and the reasons behind its unpopular design choice

#Why people feel the JavaScript fatigue, and what you can do to avoid the most common errors developers make when approaching the React ecosystem